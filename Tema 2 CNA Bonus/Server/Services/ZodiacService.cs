﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;

namespace Server.Services
{
    public class ZodiacService: Zodiacul.ZodiaculBase
    {
        public ZodiacService(){}

        public override Task<ZodieId> GetZodieId(DataNastere request, ServerCallContext context)
        {
            ZodieId zId = new ZodieId();
            string[] date = System.IO.File.ReadAllLines(@"C:\Users\Mihai\Desktop\Tema 2 CNA Bonus\Zodiac.txt");
            DateTime[] dateConv= { };int i = 0;
            DateTime dataTrimisa = DateTime.Parse(request.Data);
            foreach (string it in date)
            { dateConv[i]= DateTime.Parse(it);i++; }
            if (dataTrimisa.Month > dateConv[0].Month && dataTrimisa.Month < dateConv[1].Month &&
                dataTrimisa.Day > dateConv[0].Day && dataTrimisa.Day < dateConv[1].Day)
                zId.Id = 1;
            else
                if (dataTrimisa.Month > dateConv[1].Month && dataTrimisa.Month < dateConv[2].Month &&
                dataTrimisa.Day > dateConv[1].Day && dataTrimisa.Day < dateConv[2].Day)
                zId.Id = 2;
            else
                if (dataTrimisa.Month > dateConv[2].Month && dataTrimisa.Month < dateConv[3].Month &&
                dataTrimisa.Day > dateConv[2].Day && dataTrimisa.Day < dateConv[3].Day)
                zId.Id = 3;
            else
                if (dataTrimisa.Month > dateConv[3].Month && dataTrimisa.Month < dateConv[4].Month &&
                dataTrimisa.Day > dateConv[3].Day && dataTrimisa.Day < dateConv[4].Day)
                zId.Id = 4;
            else
                if (dataTrimisa.Month > dateConv[4].Month && dataTrimisa.Month < dateConv[5].Month &&
                dataTrimisa.Day > dateConv[4].Day && dataTrimisa.Day < dateConv[5].Day)
                zId.Id = 5;
            else
                if (dataTrimisa.Month > dateConv[5].Month && dataTrimisa.Month < dateConv[6].Month &&
                dataTrimisa.Day > dateConv[5].Day && dataTrimisa.Day < dateConv[6].Day)
                zId.Id = 6;
            else
                if (dataTrimisa.Month > dateConv[6].Month && dataTrimisa.Month < dateConv[7].Month &&
                dataTrimisa.Day > dateConv[6].Day && dataTrimisa.Day < dateConv[7].Day)
                zId.Id = 7;
            else
                if (dataTrimisa.Month > dateConv[7].Month && dataTrimisa.Month < dateConv[8].Month &&
                dataTrimisa.Day > dateConv[7].Day && dataTrimisa.Day < dateConv[8].Day)
                zId.Id = 8;
            else
                if (dataTrimisa.Month > dateConv[8].Month && dataTrimisa.Month < dateConv[9].Month &&
                dataTrimisa.Day > dateConv[8].Day && dataTrimisa.Day < dateConv[9].Day)
                zId.Id = 9;
            else
                if (dataTrimisa.Month > dateConv[9].Month && dataTrimisa.Month < dateConv[10].Month &&
                dataTrimisa.Day > dateConv[9].Day && dataTrimisa.Day < dateConv[10].Day)
                zId.Id = 10;
            else
                if (dataTrimisa.Month > dateConv[10].Month && dataTrimisa.Month < dateConv[11].Month &&
                dataTrimisa.Day > dateConv[10].Day && dataTrimisa.Day < dateConv[11].Day)
                zId.Id = 11;
            else
                if (dataTrimisa.Month > dateConv[11].Month && dataTrimisa.Month < dateConv[12].Month &&
                dataTrimisa.Day > dateConv[11].Day && dataTrimisa.Day < dateConv[12].Day)
                zId.Id = 12;
            return Task.FromResult(zId);
        }
        public override Task<Zodie> GetZodie(ZodieId request, ServerCallContext context)
        {
            Zodie ZodiaTa1 = new Zodie();
            if (request.Id == 1)
                ZodiaTa1.ZodiaTa = "Berbec";
            else
                if (request.Id == 2)
                ZodiaTa1.ZodiaTa = "Taur";
            else
                if (request.Id == 3)
                ZodiaTa1.ZodiaTa = "Gemeni";
            else
                if (request.Id == 4)
                ZodiaTa1.ZodiaTa = "Rac";
            else
                if (request.Id == 5)
                ZodiaTa1.ZodiaTa = "Leu";
            else
                if (request.Id == 6)
                ZodiaTa1.ZodiaTa = "Fecioara";
            else
                if (request.Id == 7)
                ZodiaTa1.ZodiaTa = "Balanta";
            else
                if (request.Id == 8)
                ZodiaTa1.ZodiaTa = "Scorpion";
            else
                if (request.Id == 9)
                ZodiaTa1.ZodiaTa = "Sagetator";
            else
                if (request.Id == 10)
                ZodiaTa1.ZodiaTa = "Capricorn";
            else
                if (request.Id == 11)
                ZodiaTa1.ZodiaTa = "Varsator";
            else
                if (request.Id == 12)
                ZodiaTa1.ZodiaTa = "Pesti";
            return Task.FromResult(ZodiaTa1);
        }

        public override Task<Anotimp> SendAnotimp(ZodieId request, ServerCallContext context)
        {
            Anotimp A = new Anotimp();
            if (request.Id >= 1 && request.Id<=3)
            {                
                A.AnotimpulZodiei=Primavara(request, context).ToString();
                 }
            else
                if (request.Id >= 4 && request.Id<=6)
                A.AnotimpulZodiei = Vara(request, context).ToString();
            else
                if (request.Id == 7 && request.Id<=9)
                A.AnotimpulZodiei = Toamna(request, context).ToString();
            else
                if (request.Id == 10 && request.Id<=12)
                A.AnotimpulZodiei = Iarna(request, context).ToString();
            return Task.FromResult(A);
        }
        public override Task<Anotimp> Primavara(ZodieId request, ServerCallContext context)
        {
            Anotimp a = new Anotimp();
            a.AnotimpulZodiei = "Primavara";
            return Task.FromResult(a);
        }

        public override Task<Anotimp> Vara(ZodieId request, ServerCallContext context)
        {
            Anotimp a = new Anotimp();
            a.AnotimpulZodiei = "Vara";
            return Task.FromResult(a);
        }
        public override Task<Anotimp> Toamna(ZodieId request, ServerCallContext context)
        {
            Anotimp a = new Anotimp();
            a.AnotimpulZodiei = "Toamna";
            return Task.FromResult(a);
        }
        public override Task<Anotimp> Iarna(ZodieId request, ServerCallContext context)
        {
            Anotimp a = new Anotimp();
            a.AnotimpulZodiei = "Iarna";
            return Task.FromResult(a);
        }
       
    }
}
