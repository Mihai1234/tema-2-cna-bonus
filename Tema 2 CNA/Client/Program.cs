﻿using Grpc.Net.Client;
using Server;
using System;
using System.Threading.Tasks;
using Grpc.Core;


namespace Client
{
    class Program
    {
        static System.DateTime Data() {
            int an=0, luna=0, zi=0;
            string val;
            
            bool ok = false, oka = false, okl = false, okz = false;
            while (!ok)
            {

                while (oka != true)
                {
                    Console.WriteLine("Ïntroduceti anul:\n");
                    val = Console.ReadLine();
                    if (Convert.ToInt32(val) > 2020)
                        Console.WriteLine("An invalid");
                    else
                    {
                        oka = true; an = Convert.ToInt32(val);
                    }
                }
                    while (okl != true)
                    {
                    Console.WriteLine("Ïntroduceti luna:\n");
                    val = Console.ReadLine();
                        if (Convert.ToInt32(val) > 12)
                            Console.WriteLine("Luna invalida");
                        else { okl = true; luna = Convert.ToInt32(val); }
                    }
                    while (okz != true)
                    {
                    Console.WriteLine("Ïntroduceti \n");
                    val = Console.ReadLine();
                        if ((Convert.ToInt32(val) > 31 && luna % 2 == 1) || (Convert.ToInt32(val) > 30 && luna % 2 == 0)
                            || (Convert.ToInt32(val) > 28 && luna == 4) || (Convert.ToInt32(val) > 29 && luna == 4 && an %4!=0))
                            Console.WriteLine("Zi invalida");
                        else { okz = true; zi = Convert.ToInt32(val); }
                    }
                if (oka && okl && okz) ok = true;
            }
            System.DateTime DataNastere = new DateTime(an,luna,zi);
            return DataNastere;
        }
        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("http://localhost:5001");string opt;
            var client = new Zodiacul.ZodiaculClient(channel);
            System.DateTime data = Program.Data();
            var dataCeruta = new DataNastere { Data = data.ToString() };
            
            var idPrimit = await client.GetZodieIdAsync(dataCeruta);
            var idPrimitConvert = Convert.ToInt32(idPrimit);
            Console.WriteLine("Doriti zodia?(0-nu,1-da) \n");
            opt = Console.ReadLine();
            if (opt == "da")
            {
                var id = new ZodieId { Id = idPrimitConvert };
                var zodie = await client.GetZodieAsync(id);
            }
        }
    }
}
